ifeq ($(OS),Windows_NT)
    RM      := del
else
    RM      := rm -rf
endif


SOURCE := \
src/main.c 

INCLUDE := 

OBJECTS := $(notdir $(SOURCE:.c=.o))

EXE := RAMTest.exe

NIL :=
SPACE := $(NIL) $(NIL)
VPATH := $(subst $(SPACE),:,$(dir $(SOURCE)))

GCC_FLAGS := $(addprefix -I , $(INCLUDE)) -O1 -pthread -Wall

all: $(EXE)
	@$(RM) $(OBJECTS)
	@echo Building done successful

start: all
	@./$(EXE)

%.o: %.c makefile
	g++ $(GCC_FLAGS) -c -o $@ $<

$(EXE): $(OBJECTS) makefile
	g++ $(GCC_FLAGS) -o $@ $(OBJECTS)