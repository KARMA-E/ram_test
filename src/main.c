#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <pthread.h>
#include <time.h>



struct threadArg_s {
	uint8_t index;
	uint32_t size;
	uint32_t time;
	uint32_t errCnt;
};

void * MemTestThread(void * vargp)
{
	uint64_t threadBeginTime = clock();

	struct threadArg_s * thArg = (struct threadArg_s*)vargp;
	uint32_t * memBuf = (uint32_t*)malloc(thArg->size);
    printf("Start thread #%u [%016llX - %016llX]\n", thArg->index, (uint64_t)memBuf, (uint64_t)memBuf + thArg->size);

    for(uint32_t j = 0; j < thArg->size / sizeof(uint32_t); j++)
    {
        memBuf[j] = (j * 0x123) & 0xFFFFFFFF;
    } 

    uint32_t iteration = 1;
     
    while((clock() - threadBeginTime) / 1000 < thArg->time)
    {
        uint64_t startTime, stopTime;

        startTime = clock();

        for(uint32_t j = 0; j < thArg->size / sizeof(uint32_t); j++)
        {
            uint32_t ramVal = memBuf[j];
            uint32_t randVal = (j * 0x123 * iteration) & 0xFFFFFFFF;

            if(ramVal != randVal)
            {
				thArg->errCnt++;
                printf("Thread #%u ERR!!! addr %llX - %08X != %08X mask %08X\n",
                        thArg->index, (uint64_t)(&memBuf[j]), ramVal, randVal, ramVal ^ randVal);
            }

            memBuf[j] = (j * 0x123 * (iteration + 1)) & 0xFFFFFFFF;
        }  

        stopTime = clock();

        if(iteration % 10 == 0)
        {
            printf("Thread %u speed %u MB/s\n", thArg->index, (thArg->size / 1024) / (uint32_t)(stopTime - startTime));
        }

        iteration++;
    }

    free(memBuf);
    printf("Thread #%u complete %u iterations with %u errors\n", thArg->index, iteration, thArg->errCnt);
    return NULL;
}


int main()
{
    uint32_t threadsQty;
    printf("Enter threads qty: ");
    scanf("%u", &threadsQty);

    uint32_t memPerThread;
    printf("Enter memory size per thread [MB/s]: ");
    scanf("%u", &memPerThread);
    memPerThread *= 1024 * 1024;

    uint32_t testingTime;
    printf("Enter testing time [s]: ");
    scanf("%u", &testingTime);

    pthread_t threadId[threadsQty];
    struct threadArg_s thArgs[threadsQty];

    for(uint8_t thrInd = 0; thrInd < threadsQty; thrInd++)
    {
    	thArgs[thrInd].index = thrInd;
    	thArgs[thrInd].size = memPerThread;
    	thArgs[thrInd].time = testingTime;
    	thArgs[thrInd].errCnt = 0;

        pthread_create(&threadId[thrInd], NULL, MemTestThread, (void*)&thArgs[thrInd]);
    }

//    system("pause");
//
//    uint32_t * victimMem = (uint32_t*)malloc(1024 * sizeof(uint32_t)) + 100000;
//    printf("Victim mem addr %llX\n", (uint64_t)victimMem);
//    *victimMem = 13;

    for(uint8_t thrInd = 0; thrInd < threadsQty; thrInd++)
	{
		pthread_join(threadId[thrInd], NULL);
	}

    system("pause");

    return 0;
}
